import React, {Fragment} from 'react';
import Button from "@material-ui/core/Button/Button";
import {Link} from 'react-router-dom'

const UserMenu = ({user, logout}) => {
    return (
        <Fragment>
            <h4>Hello, {user.username}</h4>
            <Button color="inherit" component={Link} to="/add">Add</Button>
            <Button color="inherit" onClick={logout}>Logout</Button>
        </Fragment>
    );
};

export default UserMenu;