import React, {Fragment} from 'react';
import Button from "@material-ui/core/Button/Button";
import {Link} from "react-router-dom";

const AnonymousMenu = () => {
    return (
        <Fragment>
            <Button color="inherit" component={Link} to="/registration">Register</Button>
            <Button color="inherit" component={Link} to="/login">Login</Button>
        </Fragment>
    );
};

export default AnonymousMenu;