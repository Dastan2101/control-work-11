import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from 'react-notifications';

export const FETCH_GOODS_SUCCESS = 'FETCH_GOODS_SUCCESS';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_GOODS_FROM_CATEGORY = 'FETCH_GOODS_FROM_CATEGORY';

export const FETCH_GOODS_INFO_SUCCESS = 'FETCH_GOODS_INFO_SUCCESS';

const fetchGoodsSuccess = data => ({type: FETCH_GOODS_SUCCESS, data});
const fetchCategoriesSuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});
const fetchGoodsFromCategory = data => ({type: FETCH_GOODS_FROM_CATEGORY, data});

const fetchGoodsInfoSuccess = data => ({type: FETCH_GOODS_INFO_SUCCESS, data});

export const getGoods = () => {
    return dispatch => {
        return axios.get('/goods').then(
            response => dispatch(fetchGoodsSuccess(response.data))
        )
    }
};

export const getCategories = () => {
    return dispatch => {
        return axios.get('category').then(
            response => dispatch(fetchCategoriesSuccess(response.data))
        )
    }
};

export const getGoodsFromCategory = (id) => {
    return dispatch => {
        return axios.get('/category/' + id).then(
            response => dispatch(fetchGoodsFromCategory(response.data))
        )
    }
};

export const createNewGoods = (data) => {
    return (dispatch, getState) => {
        if (getState().users.user.token) {
            const token = getState().users.user.token;
            return axios.post('/goods', data, {headers: {"Authorization": token}}).then(
                () => {
                    NotificationManager.success('Success created!');
                    dispatch(push('/'))
                }

            );

        } else {
            NotificationManager.error('Not authorized!');
            dispatch(push('/login'))
        }
    }
};

export const getGoodsInfo = id => {
    return dispatch => {
        return axios.get('/goods/' + id).then(
            response => dispatch(fetchGoodsInfoSuccess(response.data))
        )
    }
};

export const deleteGoods = id => {
    return (dispatch, getState) => {
        if (getState().users.user.token) {
            const token = getState().users.user.token;
            return axios.delete('/goods/' + id, {headers: {"Authorization": token}}).then(
                response => {
                    getGoods();
                    NotificationManager.success('Success deleted!');
                    dispatch(push('/'))
                }
            )
        }

    }
};
