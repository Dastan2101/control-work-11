import {
    FETCH_CATEGORIES_SUCCESS,
    FETCH_GOODS_FROM_CATEGORY,
    FETCH_GOODS_INFO_SUCCESS,
    FETCH_GOODS_SUCCESS
} from "../actions/Actions";

const initialState = {
    goods: null,
    categories: [],
    goodsInfo: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GOODS_SUCCESS:
            return {...state, goods: action.data};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories};
        case FETCH_GOODS_FROM_CATEGORY:
            return {...state, goods: action.data};
        case FETCH_GOODS_INFO_SUCCESS:
            return {...state, goodsInfo: action.data};
        default:
            return state;
    }
};

export default reducer;
