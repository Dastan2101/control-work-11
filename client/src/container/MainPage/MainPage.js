import React, {Component} from 'react';
import {getCategories, getGoods, getGoodsFromCategory} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button/Button";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    card: {
        width: 250,
        margin: 10,
        display: 'inline-block',
        textAlign: 'center'
    },
    media: {
        width: 200,
        height: 300,
        backgroundSize: 'curtain',
        margin: '15px auto'
    },
    button: {
        margin: theme.spacing.unit,
    }
});


class MainPage extends Component {

    componentDidMount() {
        this.props.getGoods();
        this.props.getCategories()
    }

    render() {
        const {classes} = this.props;
        let goods;

        if (this.props.goods) {
            goods = this.props.goods.map(item => (
                <Link to={'/information/' + item._id} key={item._id}>
                    <Card className={classes.card}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={'http://localhost:8000/uploads/' + item.image}
                                title="image"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {item.title}
                                </Typography>
                                <Typography component="p">
                                    {item.price} $
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Link>
            ))
        }


        return (
            <div className={classes.root}>
                <Grid container spacing={24}>

                    <Grid item sm={4}>
                        <Paper className={classes.paper}
                        >< Button variant="outlined" color="inherit" className={classes.button}
                                  onClick={() => this.props.getGoods()}>
                            All categories
                        </Button></Paper>
                        {this.props.categories.map(category => (
                            <Paper className={classes.paper}
                            key={category._id}
                            >
                                < Button variant="outlined" color="inherit" className={classes.button}
                                      onClick={() => this.props.getGoodsFromCategory(category._id)}>
                                {category.title}
                            </Button>
                            </Paper>
                        ))}
                    </Grid>
                    <Grid item sm={8}>
                        {goods}
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    goods: state.goods.goods,
    categories: state.goods.categories
});

const mapDispatchToProps = dispatch => ({
    getGoods: () => dispatch(getGoods()),
    getCategories: () => dispatch(getCategories()),
    getGoodsFromCategory: id => dispatch(getGoodsFromCategory(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainPage));