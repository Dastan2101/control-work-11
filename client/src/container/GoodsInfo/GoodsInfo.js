import React, {Component} from 'react';
import {deleteGoods, getGoodsInfo} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';


const styles = theme => ({
    card: {
        maxWidth: 400,
        maxHeight: 700,
        margin: '0 auto'
    },
    media: {
        paddingTop: '56.25%',
        backgroundSize: 'curtain'
    },
    button: {
        margin: theme.spacing.unit,
    }
});

class GoodsInfo extends Component {

    componentDidMount() {
        this.props.getGoodsInfo(this.props.match.params.id)
    }

    render() {

        const {classes} = this.props;

        let button;

        if (this.props.user && this.props.info && this.props.info.user) {
            if (this.props.user._id === this.props.info.user._id) {

                button = <IconButton className={classes.button} aria-label="Delete"
                                     onClick={() => this.props.deleteGoods(this.props.info._id)}>
                    <DeleteIcon/>
                </IconButton>
            }
        }

        return (
            <div>
                {this.props.info ? <Card className={classes.card}>
                    <CardHeader
                        title={'user: ' + this.props.info.user.displayName}
                        subheader={'phone: ' + this.props.info.user.phoneNumber}
                    />
                    <CardMedia
                        className={classes.media}
                        image={'http://localhost:8000/uploads/' + this.props.info.image}
                    />
                    <CardContent>
                        <Typography component="p">
                            Title:
                            {this.props.info.title}
                        </Typography>
                        <Typography component="p">
                            Price:
                            {this.props.info.price}
                            $
                        </Typography>
                    </CardContent>
                    <CardContent>
                        <Typography component="p">
                            Description:
                            {this.props.info.description}
                        </Typography>
                    </CardContent>
                    <CardContent>
                        <Typography component="p">
                            Category:
                            {this.props.info.category.title}
                        </Typography>
                    </CardContent>
                    {button}
                </Card> : null}
            </div>

        );
    }
}

const mapStateToProps = state => ({
    info: state.goods.goodsInfo,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getGoodsInfo: id => dispatch(getGoodsInfo(id)),
    deleteGoods: id => dispatch(deleteGoods(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(GoodsInfo));