import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import {withStyles} from '@material-ui/core/styles';
import {connect} from "react-redux";
import {createNewGoods, getCategories} from "../../store/actions/Actions";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        alignSelf: 'center',
        alignItems: 'center',
        width: 500,
        flexDirection: 'column'

    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 400,
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    }
});

class AddGoodsForm extends Component {

    componentDidMount() {
        this.props.getCategories()
    }

    state = {
        title: '',
        description: '',
        price: '',
        image: '',
        category: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {

        if (this.state.price > 0) {

            event.preventDefault();

            const formData = new FormData();

            Object.keys(this.state).forEach(key => {
                formData.append(key, this.state[key]);
            });

            this.props.createNewGoods(formData);
        }

    };

    render() {
        const {classes} = this.props;

        return (
            <form className={classes.container} noValidate autoComplete="off" style={{margin: '50px auto'}}>

                <TextField
                    label="Title"
                    placeholder="Title"
                    className={classes.textField}
                    required
                    margin="normal"
                    name="title"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                />

                <TextField
                    label="Description"
                    placeholder="Description"
                    className={classes.textField}
                    margin="normal"
                    required
                    name="description"
                    value={this.state.description}
                    onChange={this.inputChangeHandler}
                />
                <TextField
                    label="Price"
                    value={this.state.price}
                    name="price"
                    onChange={this.inputChangeHandler}
                    type="number"
                    required
                    className={classes.textField}
                    margin="normal"
                />

                <TextField
                    select
                    className={classes.textField}
                    required
                    name="category"
                    value={this.state.category}
                    onChange={this.inputChangeHandler}
                    SelectProps={{
                        native: true,
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    helperText="Please select your category"
                    margin="normal"
                >
                    {this.props.categories.map(option => (
                        <option key={option._id} value={option._id} defaultValue="">
                            {option.title}
                        </option>
                    ))}
                </TextField>
                <input
                    type="file"
                    name="image"
                    required
                    onChange={this.fileChangeHandler}
                    style={{margin: '15px auto'}}
                />
                <Button variant="contained" color="default" className={classes.button} onClick={this.submitFormHandler}>
                    Upload
                    <CloudUploadIcon className={classes.rightIcon}/>
                </Button>

            </form>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.goods.categories
});

const mapDispatchToProps = dispatch => ({
    createNewGoods: data => dispatch(createNewGoods(data)),
    getCategories: () => dispatch(getCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddGoodsForm));