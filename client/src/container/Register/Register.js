import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField/TextField";
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Icon from '@material-ui/core/Icon';
import FormGroup from "@material-ui/core/FormGroup/FormGroup";
import Modal from '@material-ui/core/Modal';
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography/Typography";
import {registerUser} from "../../store/actions/usersActions";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: 250,
        margin: '0 auto'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
        alignSelf: 'center'
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    paper: {
        position: 'absolute',
        top: 100,
        left: '50%',
        marginLeft: -250,
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    }
});

class Register extends Component {

    state = {
        username: '',
        password: '',
        displayName: '',
        phoneNumber: '',
        open: true
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const data = {
            username: this.state.username,
            password: this.state.password,
            displayName: this.state.displayName,
            phoneNumber: this.state.phoneNumber,
        };

        this.props.registerUser(data);

    };

    handleClose = () => {
        this.setState({open: false});
        this.props.history.push('/')
    };

    render() {
        const {classes} = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
            >

                <div className={classes.paper}>
                    <Typography variant="h6" id="modal-title">
                        Registration
                    </Typography>
                    <FormGroup className={classes.container}>
                        <TextField
                            label="Name"
                            type="text"
                            name="username"
                            required
                            className={classes.textField}
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            margin="normal"
                            error={this.props.error && true}
                            helperText={this.props.error && this.props.error.errors.username && this.props.error.errors.username.message}
                            autoComplete="new-username"
                        />

                        <TextField
                            label="Password"
                            className={classes.textField}
                            type="password"
                            name="password"
                            required
                            autoComplete="new-password"
                            error={this.props.error && true}
                            helperText={this.props.error && this.props.error.errors.password && this.props.error.errors.password.message}
                            margin="normal"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                        />
                        <TextField
                            label="Display name"
                            className={classes.textField}
                            type="text"
                            name="displayName"
                            required
                            autoComplete="new-displayName"
                            margin="normal"
                            value={this.state.displayName}
                            onChange={this.inputChangeHandler}
                        />
                        <input
                            placeholder="Phone number"
                            className={classes.textField}
                            type="tel"
                            pattern="3[0-9]{3}-[0-9]{3}"
                            name="phoneNumber"
                            required
                            autoComplete="new-phoneNumber"
                            value={this.state.phoneNumber}
                            onChange={this.inputChangeHandler}
                            style={{height: '30px'}}
                        />
                        <Button variant="contained" color="secondary" className={classes.button}
                                onClick={this.submitFormHandler}>
                            Save
                            <Icon className={classes.rightIcon}>save</Icon>
                        </Button>
                    </FormGroup>
                </div>
            </Modal>

        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Register));