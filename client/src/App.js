import React, {Component, Fragment} from 'react';
import Navigation from "./components/Navigation/Navigation";
import {Route, Switch, withRouter} from "react-router-dom";
import MainPage from "./container/MainPage/MainPage";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/usersActions";
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import AddGoodsForm from "./container/AddGoodsForm/AddGoodsForm";
import GoodsInfo from "./container/GoodsInfo/GoodsInfo";
import {NotificationContainer} from "react-notifications";

class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <header>
                    <Navigation
                        user={this.props.user}
                        logout={this.props.logoutUser}
                    />
                </header>
                <div style={{width: '1200px', margin: '50px auto'}}>
                    <Switch>
                        <Route path="/" exact component={MainPage}/>
                        <Route path="/information/:id" exact component={GoodsInfo}/>
                        <Route path="/registration" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/add" exact component={AddGoodsForm}/>
                    </Switch>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
