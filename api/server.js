const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const goods = require('./app/goods');
const category = require('./app/category');
const users = require('./app/user');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

    app.use('/users', users);
    app.use('/goods', goods);
    app.use('/category', category);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});
