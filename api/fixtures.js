const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Goods = require('./models/Goods');
const Category = require('./models/Category');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'Dastan',
            displayName: 'D12',
            phoneNumber: '+996 555 555 555',
            password: '123',
            token: '123456'
        },
        {
            username: 'Samat',
            displayName: 'Sam',
            phoneNumber: '+996 123 456 789',
            password: '111',
            token: '654321'
        },
        {
            username: 'Timur',
            displayName: 'Tma',
            phoneNumber: '+996 123 455 111',
            password: '111',
            token: '963258'
        }
    );

    const category = await Category.create(
        {
            title: "Computer"
        },
        {
            title: "Mobile"
        },
        {
            title: "Gadget"
        },
        {
            title: "Others"
        }
    );

    await Goods.create(
        {
            user: user[0]._id,
            category: category[0]._id,
            title: 'Acer Predator',
            description: 'Acer Predator Helios 300 (HERO) PH317-51-71YP, Black',
            price: 350,
            image: 'acer_predator.jpg'
        },
        {
            user: user[1]._id,
            title: 'iPhone XS',
            description: 'iPhone XS Max Dual Sim',
            price: 500,
            image: 'iPhone_XS.jpeg',
            category: category[1]._id
        },
        {
            user: user[2]._id,
            title: 'APPLE WATCH ',
            description: 'APPLE WATCH SERIES 4 + VIVO SYNC',
            price: 420,
            image: 'iwatch.jpg',
            category: category[2]._id
        },
        {
            user: user[2]._id,
            title: 'Oxygen',
            description: 'Oxygen Eyeglasses Black',
            price: 110,
            image: 'Oxygen.jpeg',
            category: category[3]._id
        }
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});