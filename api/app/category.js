const express = require('express');

const Category = require('../models/Category');
const Goods = require('../models/Goods');

const router = express.Router();

router.get('/', (req, res) => {
    Category.find()
        .then(categories => res.send(categories))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
    Goods.find({category: req.params.id}).populate("goods")
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});


module.exports = router;
