const express = require('express');
const Users = require('../models/User');

const router = express.Router();

router.post('/', (req, res) => {
    const user = new Users(req.body);
    user.generateToken();
    user.save()
        .then(user => res.send({message: 'User registered', user}))
        .catch(error => res.status(400).send(error))
});

router.post('/sessions', async (req, res) => {
    const user = await Users.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: "Incorrect login or password"});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: "Incorrect login or password"});
    }

    user.generateToken();

    await user.save();

    return res.send({message: "Login success", user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get("Authorization");

    const success = {message: "Success"};

    if (!token) {
        return res.send(success)
    }

    const user = await Users.findOne({token});

    if (!user) {
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success);
});

module.exports = router;