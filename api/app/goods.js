const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const auth = require('../midleware/auth');

const Goods = require('../models/Goods');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Goods.find()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});

router.post('/', [upload.single('image'), auth], (req, res) => {
    const goodsData = req.body;

    if (req.file) {
        goodsData.image = req.file.filename;
    }

    goodsData.user = req.user._id;
    const goods = new Goods(goodsData);

    goods.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.get('/:id', (req, res) => {
    Goods.findById(req.params.id).populate("user").populate("category")
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});

router.delete('/:id', auth, (req, res) => {

    Goods.findById(req.params.id).then(
        result => {
            if (!result.user.equals(req.user._id)) {
                return res.status(403).send({message: "You cannot delete this position!"})
            } else {
                result.remove();
                res.send({message: "Success deleted!"})
            }
        }
    ).catch(error => res.sendStatus(error))


});

module.exports = router;